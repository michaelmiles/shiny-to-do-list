library(shiny)

# Define starting UI (eventually move this to a module)
ui <- fluidPage(
  actionButton("action_add_task", 
               "Create New Task"),
  textInput("text", 
            label = h3(""), 
            value = "Enter task name...")
)

# Needs for server fuction
# Ability to:
# - Insert and remove UIs with insertUI(), removeUI()
# - renderUI()?
# - How do we duplicate existing UI code? Call module?
# - Way to put the current datetime and task name into a UI


# Smaller UIs will have
# - Action buttons to edit, delete - actionButton()
# - Text box - renderText()?


# Define server - empty for now
server <- function(input, output, session) {
}


# Run shiny app
shinyApp(ui, server)
